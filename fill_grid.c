/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_grid.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: randrini <randrini@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 16:19:48 by randrini          #+#    #+#             */
/*   Updated: 2016/02/04 00:29:13 by randrini         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

int		size_col(char *str)
{
	int	i;
	int	j;

	i = 0;
    j = 0;
	while (str[i] != '\n')
		i++;
	i += 1; // skip '\n'
	while (str[i] != '\n')
    {
		j++;
        i++;
    }
	return (j);
}

int		size_row(char *str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			j++;
		i++;
	}
	j -= 1;
	return (j);
}

int		skip_first_line(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\n')
		i++;
	i += 1;
	return (i);
}

void	solve_grid(char **grid, int max_row, int max_col)
{
	int     i;
	int     j;
	int     max_of_s;
	int     max_i;
	int     max_j;
	char	sub[max_row][max_col];
	
    i = 0;
	while (i < max_row) // set first column of sub[][]
	{
		sub[i][0] = grid[i][0];
		i++;
	}
	j = 0;
	while (j < max_col) // set first row of sub
	{
		sub[0][j] = grid[0][j];
		j++;
	}
	max_of_s  = sub[0][0];
	max_i = 0;
	max_j = 0;
	i = 1;
	while (i < max_row)
	{
		j = 1;
		while (j < max_col)
		{
			if (grid[i][j] == 1)
				sub[i][j] = ft_min(sub[i][j - 1], sub[i - 1][j],
                        sub[i - 1][j - 1]) + 1;
			else
				sub[i][j] = 0;
			if (sub[i][j] > max_of_s)
			{
				max_of_s  = sub[i][j];
				max_i = i;
				max_j = j;
			}
			j++;
		}
		i++;
	}
	print_solved_grid(grid, max_row, max_col, max_i, max_j, max_of_s);
}

void	print_solved_grid(char **grid, int	max_row, int max_col,
        int max_i, int max_j, int	max_of_s)
{
	int i;
	int j;
	
    i = 0;
	while (i < max_row)
	{
		j = 0;
		while (j < max_col)
		{
			if (grid[i][j] == 1)
			{
				grid[i][j] = '.';
			}
			if (grid[i][j] == 0)
				grid[i][j] = 'o';
			if ((i < max_i + 1 && i > max_i - max_of_s) && (j <= max_j &&
                        j > max_j - max_of_s))
				grid[i][j] = 'x';
		    ft_putchar(grid[i][j]);
			j++;;
		}
		ft_putchar('\n');
		i++;
	}
}

void	convert_grid(char **grid, int max_row, int max_col)
{
	int i;
	int j;

	i = 0;
	while (i < max_row)
	{
		j = 0;
		while (j < max_col)
		{
			if (grid[i][j] == '.')
				grid[i][j] = 1;
			if(grid[i][j] == 'o')
				grid[i][j]  = 0;
			j++;
		}
		i++;
	}
	solve_grid(grid, max_row, max_col);
}

void	fill_grid(char *str)
{
	char	**grid;
	int		max_row;
	int		max_col;
	int		row;
	int		col;
	int		i;

	max_row = size_row(str);
	max_col = size_col(str);
	row = 0;
	i = skip_first_line(str);
	grid = malloc(sizeof(char *) * max_row);
	while (row < max_row)
	{
		grid[row] = malloc(sizeof(char) * max_col);
		row++;
	}
	row = 0;
	while (row < max_row)
	{
		col = 0;
		while (col < max_col)
		{
			grid[row][col] = str[i];
			col++;
			i++;
		}
		i += 1;
		row++;
	}
	convert_grid(grid, max_row, max_col);
}
