#include "lib.h"

int ft_error(void)
{
    ft_putstr("map error\n");
    exit (1);
}

char    *first_line(char *buf)
{
    char    *first_line;
    int i;

    i = 0;
    printf("BUF[0] %c \n", buf[0]);
    if (buf[0] == '\0')
        ft_error();
    while (buf[i] != '\n')
        i++;
    first_line = (char*)malloc(sizeof(char) * (i + 1));
    i = 0;
    printf("BUF[0] %c \n", buf[0]);
    while (buf[i] != '\n')
    {
        first_line[i] = buf[i];
        i++;
    }
    first_line[i] = '\0';
    return (first_line);
}

int     ft_strlen(char *str)
{
    int i;

    i = 0;
    while (str[i])
        i++;
    return (i);
}

int     ft_intlen(int n)
{

    int i;
    
    i = 0;
    while ( n != 0)
    {
        n /= 10;
        ++i;
    }
    return (i);
}

void    ft_check_file(char *buf)
{
    int i;
    char *str;
    //int number;

    i = 0;
    str = first_line(buf);
    printf("STR[0] %c \n", str[0]);
    if (str[0] >= 48 && str[0] <= 57)
    {
        //number = ft_atoi(str);
        printf("ATOI(STR) %d \n", ft_atoi(str));
        printf("GRID WIDTH %d \n", size_col(buf));
        if (ft_atoi(str) != size_col(buf))
            ft_error();
        i = ft_intlen(ft_atoi(str));
        
        printf("I AT '.' %d \n", i);
        printf("VALUE OF STR at I %c \n", str[i]);
        
        if (str[i] == '.')
            i++;
        if (str[i] == 'o')
            i++;
        if (str[i] == 'x')
            i++;
        else
            ft_error();
    }
    else
        ft_error();
             
}

void    ft_check_grid(char *str)
{
    int i;
    int j;;
    int k;

    i = 0;
    j = 0;
    while (str[i] != '\n')
        i++;
    i += 1;
    while (str[++i] != '\n' && str[i] != '\0')
        j++;
    i += 1;
    k = i;
    while (str[i] != '\0')
    {
        while (str[i] != '\n' && str[i] != '\0')
            ++i;
        if ((i - 1 - k) != j)
            ft_error();
        else
        {
           i++;
            k = i;
        }
    }
}

int ft_check_full_o(char *str)
{
    int i;
    
    i = 0;
    while (str[i] != '\n')
        i++;
    i++;
    while (str[i] != '\0')
    {
        if (str[i] == 'o' || str[i] == '\n')
            i++;
        else
            return (0);
    }
    ft_error();
    return (1);
}

char    *parse_file(char *file)
{
    int         fd;
    int         ret;
    static char buf[BUFFER_SIZE];

    fd = open(file, O_RDONLY);
    if (fd == -1)
        ft_error();
    ret = read(fd, buf, BUFFER_SIZE);
    buf[ret] = '\0';
    close(fd);
    ft_check_file(buf);
    ft_check_full_o(buf);
    ft_check_grid(buf);
    return (buf);
}
