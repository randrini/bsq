/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: randrini <randrini@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 15:37:41 by randrini          #+#    #+#             */
/*   Updated: 2016/02/04 00:25:22 by randrini         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIB_H
# define LIB_H

# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdio.h>

# define BUFFER_SIZE 4096000000

char    *parse_file(char *file);
void    fill_grid(char *str);
void    ft_putchar(char c);
void    ft_putstr(char *str);
int     size_row(char *str);
int     size_col(char *str);
void    solve_grid(char **grid, int row, int col);
void    print_solved_grid(char **grid, int	max_row, int max_col, int max_i,
        int max_j, int	max_of_s);
int     ft_min(int, int, int);
int     ft_error(void);
void    ft_check_file(char *str);
int     ft_check_full_o(char *str);

#endif
